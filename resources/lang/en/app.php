<?php

return [

    /*
    |--------------------------------------------------------------------------
    | App Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'created_at' => 'Created at',
    'client' => 'Client',
    'clients' => 'Clients',

    'course' => 'Course',
    'courses' => 'Courses',

    'course_request' => 'Course request',
    'course_requests' => 'Course requests',

    'location' => 'Location',
    'locations' => 'Locations',

    'list' => 'List',

    'first_name' => 'First name',
    'last_name' => 'Last name',
    'phone' => 'Phone',
    'address' => 'Address',
    'actions'=> 'Actions',
    'edit'=> 'Keisti',
    'create' => 'Kurti',
    'new' => 'Naujas',
    'new_entry' => 'New entry',
    'users_list' => 'Users list',
    'users' => 'Users',
    'user' => 'User',

    'enter_first_name' => 'Enter first name',
    'enter_last_name' => 'Enter last name',
    'enter_phone' => 'Enter phone',
    'enter_email' => 'Enter email',
    'enter_address' => 'Enter address',
    'enter_password' => 'Enter password',

    'email' => 'email',
    'password' => 'password',
    'submit' => 'Submit',

    'title'=> 'Title',
    'enter_title'=> 'Enter title',
    'description'=> 'Description',
    'enter_description'=> 'Enter description',

    'logout' => 'Logout',
];
